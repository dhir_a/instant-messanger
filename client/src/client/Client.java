package client;
import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class Client extends JFrame{
	private JTextField text;
	private JTextArea chatwindow;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private String message="";
	private String serverip;
	private Socket connection;
	public Client(String host)
	{
		super("CLIENT WINDOW");
		serverip=host;
		text=new JTextField();
		text.setEditable(false);
		text.addActionListener(
				new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						sendmessage(e.getActionCommand());
						text.setText("");
						
					}
				});
		chatwindow=new JTextArea();
		add(text,BorderLayout.NORTH);
		add(new JScrollPane(chatwindow),BorderLayout.CENTER);
		setSize(300,300);
		setVisible(true);
	}
	public void startrunning()
	{
		try{
			setupconnection();
			setupstreams();
			whilechatting();
		}
		catch(EOFException eofException)
		{
			showmessage("\n CONNECTION TO SERVER TERMINATED");
		}
		catch(IOException ioException)
		{
			ioException.printStackTrace();
		}
		finally
		{
			closecrap();
		}
	}
	private void setupconnection() throws IOException
	{
		showmessage("CONNECTING TO SERVER...");
		connection=new Socket(InetAddress.getByName(serverip),6789);
		showmessage("CONNECTED TO: "+connection.getInetAddress().getHostName());
		
	}
	private void setupstreams() throws IOException
	{
		output=new ObjectOutputStream(connection.getOutputStream());
		output.flush();
		input=new ObjectInputStream(connection.getInputStream());
	    showmessage("STREAMS CONNECTED");
	    
	}
	private void whilechatting() throws IOException
	{
	    abletotype(true);
	    do{
	    	try{
	    		message=(String)input.readObject();
	    		showmessage("\n"+message);
	    	}
	    	catch(ClassNotFoundException classNotFoundException)
	    	{
	    		showmessage("OBJECT TYPE NOT RECOGANISED");
	    	}
	    
	      }
	    while(!message.equals("SERVER - END"));
}
	private void closecrap()
	{
		showmessage("CLOSING ALL THE STUFF");
		abletotype(false);
		try
		{
			output.close();
			input.close();
		    connection.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	private void sendmessage(String s){
		try {
			output.writeObject("CLIENT - "+s);
			output.flush();
			showmessage("\nCLIENT -  "+s);
		} catch (IOException e) {
			chatwindow.append("ERROR: DUDE I CAN'T SEND THAT MESSAGE");
			e.printStackTrace();
		}
	}
	private void showmessage(final String f)
	{
		SwingUtilities.invokeLater(
				new Runnable() {
					
					@Override
					public void run() {
						chatwindow.append(f);
						
					}
				}
				);
	}
	private void abletotype(final boolean t)
	{
		SwingUtilities.invokeLater(
				new Runnable() {
					
					@Override
					public void run() {
						text.setEditable(t);
						
					}
				}
				);
	}
}
