package messenger;
import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
public class Server extends JFrame{
	private JTextField text;
	private JTextArea chatwindow;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private ServerSocket server;
	private Socket connection;
	public Server(){
		super("MY INSTANCE MESSENGER");
		text=new JTextField();
		text.setEditable(false);
		text.addActionListener(
				new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						 
						sendmessage(e.getActionCommand());
						text.setText("");
					}
				}
				);
		chatwindow=new JTextArea();
		add(text,BorderLayout.NORTH);
		add(new JScrollPane(chatwindow),BorderLayout.CENTER);
        setSize(300,300);
        setVisible(true);
	}
	// method to run the server
	void startrunning()
	{
		try{
			server =new ServerSocket(6789,100);
			while(true)
			{
				try{
			         waitforconnection();
			         setupstreams();
			         whilechatting();
			         
				   } 	
			catch(EOFException eofException){
               		showmessage("\n SERVER ENDED THE CONNECTION");		
			}
			finally{
				closecrap();
			}
		}
		}
		catch(IOException ioException)
		{
		    ioException.printStackTrace();	
		}
	}
	// method to wait for connection
	private void waitforconnection()throws IOException{
		showmessage("WAITING FOR CONNECTION.......");
		connection=server.accept();
		showmessage("CONNECTION ESTABLISHED TO :"+connection.getInetAddress().getHostName());
		
		
	}
	//setup stream to send and recieve data 
	private void setupstreams () throws IOException{
		output=new ObjectOutputStream(connection.getOutputStream());
	    output.flush();
	    input=new ObjectInputStream(connection.getInputStream());
	    showmessage("STREAM ARE READY!!!");
	}
	private void whilechatting() throws IOException{
		String message="YOU ARE NOW CONNECTED";
		sendmessage(message);
		abletotype(true);
		do{
			
			try
			{
				message=(String)input.readObject();
				showmessage("\n"+message);
			}
			catch(ClassNotFoundException classNotFoundException)
			{
				showmessage("\n"+"idk what the user sent");
			}
		}
		while(!message.equals("CLIENT - END"));
	}
	// closing all the stuff
	private void closecrap()
	{
		showmessage("CLOSING THE CONNECTION");
		abletotype(false);
		try
		{
			input.close();
			output.close();
			connection.close();
		}
		catch(IOException ioException)
		{
			ioException.printStackTrace();
		}
	}
	private void sendmessage(String s){
		try {
			output.writeObject("SERVER - "+s);
			output.flush();
			showmessage("\nSERVER -  "+s);
		} catch (IOException e) {
			chatwindow.append("ERROR: DUDE I CAN'T SEND THAT MESSAGE");
			e.printStackTrace();
		}
	}
	private void showmessage(final String f)
	{
		SwingUtilities.invokeLater(
				new Runnable() {
					
					@Override
					public void run() {
						chatwindow.append(f);
						
					}
				}
				);
	}
	private void abletotype(final boolean t)
	{
		SwingUtilities.invokeLater(
				new Runnable() {
					
					@Override
					public void run() {
						text.setEditable(t);
						
					}
				}
				);
	}
   
}
